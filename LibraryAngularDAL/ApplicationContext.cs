﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using LibraryAngular.Entities;

namespace LibraryAngular.DAL
{
  public class ApplicationContext : IdentityDbContext<ApplicationUser>
  {
    public ApplicationContext(DbContextOptions<ApplicationContext> options)
        : base(options)
    {
    }
  }
}
