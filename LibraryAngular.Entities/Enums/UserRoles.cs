﻿
namespace LibraryAngular.Entities.Enums
{

  public enum UserRoles
  {
    Admin = 0,
    User = 1,
    Guest = 2
  }

}