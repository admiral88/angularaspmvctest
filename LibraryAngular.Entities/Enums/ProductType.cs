﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryAngular.Entities.Enums
{

  public enum ProductType
  {
    Books = 0,
    Magazines = 1,
    Brochures = 2
  }

}