﻿using System;

namespace LibraryAngular.Entities
{
  public class BaseEntity
  {
    public int Id { get; set; }
    public DateTime DateFrom { get; set; }
  }
}
