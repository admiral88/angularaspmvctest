﻿using Microsoft.AspNetCore.Identity;

namespace LibraryAngular.Entities
{
  public class ApplicationUser : IdentityUser 
  {
    public int Year { get; set; }
  }
}
