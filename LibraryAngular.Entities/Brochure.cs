﻿namespace LibraryAngular.Entities
{
  public class Brochure : BaseEntity
  {
    public string Name { get; set; }
    public string Genre { get; set; }
    public double Price { get; set; }
  }
}