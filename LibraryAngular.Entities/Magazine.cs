﻿namespace LibraryAngular.Entities
{
  public class Magazine:BaseEntity 
  {
    public string Name { get; set; }
    public string Genre { get; set; }
    public double Price { get; set; }
  }
}