﻿
namespace LibraryAngular.Views.EnumViewModels
{

  public enum UserRolesViewModel

  {
    Admin = 0,
    User = 1,
    Guest = 2
  }

}