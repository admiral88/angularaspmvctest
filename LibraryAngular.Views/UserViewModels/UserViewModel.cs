﻿using LibraryAngular.Views.EnumViewModels;

namespace LibraryAngular.Views.UserViewModels

{
  public class UserViewModel
  {
    public string UserName { get; set; }
    public bool IsAuth { get; set; }
    public UserRolesViewModel Role { get; set; }
    public string Password { get; set; }
  }
}
