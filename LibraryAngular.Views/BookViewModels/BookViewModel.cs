﻿using System;

namespace LibraryAngular.Views.BookViewModels
{
  public class BookViewModel
  {
    public int Id { get; set; }
    public string Author { get; set; }
    public string Name { get; set; }
    public DateTime DateFrom { get; set; }
    public string Genre { get; set; }
    public double Price { get; set; }
  }
}