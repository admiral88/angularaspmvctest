﻿using System;

namespace LibraryAngular.Views.MagazineViewModels
{
  public class MagazineViewModel
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public DateTime DateFrom { get; set; }
    public string Genre { get; set; }
    public double Price { get; set; }
  }
}