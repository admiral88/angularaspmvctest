﻿using System;


namespace LibraryAngular.Views.StoreViewModels
{
  public class StoreViewModel
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public DateTime DateFrom { get; set; }
    public string Theme { get; set; }
    public double Price { get; set; }
    public string TypeProduct { get; set; }
  }
}