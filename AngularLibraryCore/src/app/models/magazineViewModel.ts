﻿export class MagazineViewModel {
    id: number;
    name: string;
    dateFrom: Date;
    genre: string;
    price: number;
}