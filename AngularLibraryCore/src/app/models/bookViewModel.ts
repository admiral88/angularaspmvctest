export class BookViewModel
{
         id: number;
         name: string;
         author: string;
         dateFrom: Date;
         genre: string;
         price: number;
}
