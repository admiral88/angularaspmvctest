﻿export class ViewModel
{
         id: number;
         name: string;
         dateFrom: Date;
         theme: string;
         price: number;
         typeProduct: string;
}
