
export class UserAuth {
  role: UserRoles
  userName: string;
  isAuth: boolean;
}

export enum UserRoles {
  admin = 0,
  user = 1,
  guest = 2
}
