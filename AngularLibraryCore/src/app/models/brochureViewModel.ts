﻿export class BrochureViewModel {
    id: number;
    name: string;
    dateFrom: Date;
    genre: string;
    price: number;
}