import { DOCUMENT } from "@angular/platform-browser";
import { Inject } from "@angular/core";

export class AppConfig {

  public readonly apiUrl: string;

  constructor( @Inject(DOCUMENT) private document: any)
  {
    let domain = this.document.location.hostname;
    this.apiUrl = "http://" + domain
  }
    //public readonly apiUrl = 'http://localhost:51816';
};
