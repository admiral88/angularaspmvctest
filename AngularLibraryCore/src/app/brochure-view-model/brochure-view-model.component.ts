import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { State, process } from '@progress/kendo-data-query';
import { GridDataResult } from '@progress/kendo-angular-grid';
import { Observable } from 'rxjs/Observable';
import { BrochureViewModel } from '../models/brochureViewModel';
import { BrochureService } from '../service/brochure.service';
import { UserAuth, UserRoles } from '../models/user';
import { AuthenticationService } from '../service/authentication.service';




@Component({
  selector: 'app-brochure-view-model',
  templateUrl: './brochure-view-model.component.html',
  styleUrls: [
    './brochure-view-model.component.css',
    '../../../node_modules/@progress/kendo-theme-default/dist/all.css',
  ],
  encapsulation: ViewEncapsulation.None,
  providers: [BrochureService, AuthenticationService]
})
export class BrochureViewModelComponent implements OnInit {
  public role: boolean = false;
  public userName: string = "not authorized";
  public view: Observable<GridDataResult>;
  public gridState: State = {
    sort: [],
    skip: 0,
    take: 10
  };

  public editDataItem: BrochureViewModel;
  public isNew: boolean;

  constructor(private brochureService: BrochureService, private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.authenticationService.auth().subscribe(result => {
      let user: UserAuth = result;
      if (user.role === UserRoles.admin) {
        this.role = true;
      }
      if (user.userName != null) {
        this.userName = user.userName
      };
    });
    this.view = this.brochureService.map(data => process(data, this.gridState));
    this.brochureService.read();
  }

  public onStateChange(state: State) {
    this.gridState = state;
    this.brochureService.read();
  }

  public addHandler() {
    this.editDataItem = new BrochureViewModel();
    this.isNew = true;
  }

  public editHandler({ dataItem }) {
    this.editDataItem = dataItem;
    this.isNew = false;
  }

  public cancelHandler() {
    this.editDataItem = undefined;
  }

  public saveHandler(product: BrochureViewModel) {
    this.brochureService.save(product, this.isNew);
  }

  public removeHandler({ dataItem }) {
    this.brochureService.remove(dataItem);
  }

}

