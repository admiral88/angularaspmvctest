import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { BookViewModelComponent } from './book-view-model/book-view-model.component';
import { BrochureViewModelComponent } from './brochure-view-model/brochure-view-model.component';
import { MagazineViewModelComponent } from './magazine-view-model/magazine-view-model.component';
import { GridModule } from '@progress/kendo-angular-grid';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { EditBrochureComponent } from './brochure-view-model/editBrochureComponent.component';
import { DialogModule } from '@progress/kendo-angular-dialog';
import { EditMagazineComponent } from './magazine-view-model/editMagazineComponent.component';
import { EditBookComponent } from './book-view-model/editBookComponent.component';
import { HomeComponent } from './home-component/home.component';
import { LoginComponent } from './login-component/login.component';
import { AuthenticationService } from './service/authentication.service';
import { AlertService } from './service/alert.service';
import { AlertComponent } from './_directives/alert.component';
import { AppConfig } from './app.config';
import { AuthGuard } from './service/auth-guard.service';

const appRoutes: Routes = [

      {
        path: '',
        component: HomeComponent
      }
      , {
        path: 'books',
        component: BookViewModelComponent
      },
      {
        path: 'broshures',
        component: BrochureViewModelComponent
      },
      {
        path: 'magazines',
        component: MagazineViewModelComponent
      },
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'logout',
        component: LoginComponent
      },
      {
        path: 'admin',
        loadChildren: './admin-component/admin.module#AdminModule'
      }
]

@NgModule({
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    GridModule,
    ButtonsModule,
    DialogModule,
  ],
  declarations: [
    AppComponent,
    BookViewModelComponent,
    BrochureViewModelComponent,
    MagazineViewModelComponent,
    EditBrochureComponent,
    EditMagazineComponent,
    EditBookComponent,
    HomeComponent,
    LoginComponent,
    AlertComponent,
  ],
  bootstrap: [AppComponent],
  providers: [
    AppConfig,
    AuthGuard,
    AlertService,
    AuthenticationService,
  ]
})
export class AppModule { }
