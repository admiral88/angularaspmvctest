import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { BookViewModel } from '../models/bookViewModel';
import { BookService } from '../service/book.service';
import { UserAuth, UserRoles } from '../models/user';
import { GridDataResult } from '@progress/kendo-angular-grid';
import { State, process } from '@progress/kendo-data-query';
import { AuthenticationService } from '../service/authentication.service';

@Component({
  selector: 'app-book-view-model',
  templateUrl: './book-view-model.component.html',
  styleUrls: ['./book-view-model.component.css',
    '../../../node_modules/@progress/kendo-theme-default/dist/all.css',

  ],
  encapsulation: ViewEncapsulation.None,
  providers: [BookService, AuthenticationService]
})
export class BookViewModelComponent implements OnInit {


  public role: boolean = false;
  public userName: string = "not authorized";
  public view: Observable<GridDataResult>;
  public gridState: State = {
    sort: [],
    skip: 0,
    take: 10
  };

  public editDataItem: BookViewModel;
  public isNew: boolean;

  constructor(private bookService: BookService, private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.authenticationService.auth().subscribe(result => {
      let user: UserAuth = result;
      if (user.role === UserRoles.admin) {
        this.role = true;
      }
      if (user.userName != null) {
        this.userName = user.userName;
      };
    });

    this.view = this.bookService.map(data => process(data, this.gridState));
    this.bookService.read();
  }

  public onStateChange(state: State) {
    this.gridState = state;
    this.bookService.read();
  }

  public addHandler() {
    this.editDataItem = new BookViewModel();
    this.isNew = true;
  }

  public editHandler({ dataItem }) {
    this.editDataItem = dataItem;
    this.isNew = false;
  }

  public cancelHandler() {
    this.editDataItem = undefined;
  }

  public saveHandler(product: BookViewModel) {
    this.bookService.save(product, this.isNew);
  }

  public removeHandler({ dataItem }) {
    this.bookService.remove(dataItem);
  }

}

