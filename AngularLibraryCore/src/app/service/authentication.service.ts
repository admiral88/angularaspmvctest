import { Injectable, Inject } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { AppConfig } from '../app.config';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/platform-browser';


@Injectable()
export class AuthenticationService {
  constructor(private http: Http, private config: AppConfig, private httpClient: HttpClient, @Inject(DOCUMENT) private document: any)
  {
    let domain = this.document.location.hostname;
    this.url = "http://" + domain;
  }

    private url: string;

    login(username: string, password: string) {
      return this.http.post(this.config.apiUrl + '/token', { username: username, password: password })
            .map((response: Response) => {
                let user = response.json();
                if (user && user.access_token) {
                    sessionStorage.setItem('access_token', JSON.stringify(user.access_token));
                }
            });
    }

    public auth(): Observable<any> {
      let token = JSON.parse(sessionStorage.getItem('access_token'));
      var result = this.httpClient.get(this.url + "/getrole", { headers: { 'Authorization': 'Bearer ' + token } })
        .map(res => { return res; });
      return result;
    }

    public logout(): Observable<any> {
    sessionStorage.removeItem('access_token');
    var result = this.httpClient.get(this.url + "/logout")
        .map(res => { return res; });
      return result;
    }
}
