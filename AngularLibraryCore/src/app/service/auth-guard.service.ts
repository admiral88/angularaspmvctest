import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs/Observable";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { UserAuth, UserRoles } from "../models/user";
import { Router } from '@angular/router';
import { AuthenticationService } from "./authentication.service";
import { AlertService } from "./alert.service";

@Injectable()
export class AuthGuard implements CanActivate {
  public isAdmin: boolean = false;
  constructor(private authenticationService: AuthenticationService, private router: Router, private alertService: AlertService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any | boolean | Observable<boolean> | Promise<boolean> {
      return this.authenticationService.auth().map(result => {
        let user: UserAuth = result;
        if (user.role != UserRoles.admin)
        {
          this.isAdmin = false;
          this.alertService.error("Вход запрещен!");
        }
        if (user.role == UserRoles.admin)
        {
          this.isAdmin = true;
        }
          return this.isAdmin;
        }).catch(() => {
            this.router.navigate(['/login']);
            return Observable.of(false);
        });;
    }
}
