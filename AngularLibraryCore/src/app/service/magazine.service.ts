import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Http, Headers, RequestOptions, RequestMethod, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { MagazineViewModel } from '../models/magazineViewModel';
import { DOCUMENT } from '@angular/platform-browser';



@Injectable()
export class MagazineService extends BehaviorSubject<any[]> {

  //private url = "http://localhost:51816/api/magazines";
  private url: string;

  constructor(private http: HttpClient, @Inject(DOCUMENT) private document: any) {
    super([]);
    let domain = this.document.location.hostname;
    console.log(domain);
    this.url = "http://" + domain + "/api/magazines";
  }


  private data: any[] = [];

  public get() {
    return this.http.get(this.url)
      .subscribe();
  }


  public read() {
    if (this.data.length) {
      return super.next(this.data);
    }
    this.http.get(this.url).map(res => <any[]>res)
      .do(data => {
        this.data = data;
      })
      .subscribe(data => {
        super.next(data);
      });
  }

  public save(data: MagazineViewModel, isNew?: boolean) {

    if (isNew) {
      var model: MagazineViewModel = {
        id: isNew ? 0 : data.id,
        name: data.name,
        dateFrom: new Date(),
        genre: data.genre,
        price: data.price
      }

      this.reset();

      return this.http.post(this.url, model)
        .subscribe(() => this.read(), () => this.read());
    }
    if (!isNew) {
      var model: MagazineViewModel = {
        id: isNew ? 0 : data.id,
        name: data.name,
        dateFrom: new Date(),
        genre: data.genre,
        price: data.price
      }
      this.reset();

      return this.http.put(this.url + '/' + data.id, model)
        .subscribe(() => this.read(), () => this.read());
    }
  }
  public remove(data: any) {
    this.reset();

    let id: number = data.id;
    return this.http.delete(this.url + '/' + id)
      .subscribe(() => this.read(), () => this.read());
  }

  public resetItem(dataItem: any) {
    if (!dataItem) { return; }
    const originalDataItem = this.data.find(item => item.ProductID === dataItem.ProductID);
    Object.assign(originalDataItem, dataItem);
    super.next(this.data);
  }

  private reset() {
    this.data = [];
  }

  private serializeModels(data?: any): string {
    return data ? `&models=${JSON.stringify([data])}` : '';
  }
}
