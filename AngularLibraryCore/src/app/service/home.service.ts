import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Http, Headers, RequestOptions, RequestMethod, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { MagazineViewModel } from '../models/magazineViewModel';
import { DOCUMENT } from '@angular/platform-browser';



@Injectable()
export class HomeService extends BehaviorSubject<any[]> {

   // private url = "http://localhost:51816/api/home";
  private url: string;

  constructor(private http: HttpClient, @Inject(DOCUMENT) private document: any) {
    super([]);
    let domain = this.document.location.hostname;
    console.log(domain);
    this.url = "http://" + domain + "/api/home";
    }
    
    private data: any[] = [];

    public get() {
        return this.http.get(this.url)
            .subscribe();           
    }

    public read() {
        if (this.data.length) {
            return super.next(this.data);
        }
        this.http.get(this.url).map(res => <any[]>res)
            .do(data => {
                this.data = data;
            })
            .subscribe(data => {
                super.next(data);
            });
    }

    public resetItem(dataItem: any) {
        if (!dataItem) { return; }
        const originalDataItem = this.data.find(item => item.ProductID === dataItem.ProductID);
        Object.assign(originalDataItem, dataItem);
        super.next(this.data);
    }

    private reset() {
        this.data = [];
    }

    private serializeModels(data?: any): string {
        return data ? `&models=${JSON.stringify([data])}` : '';
    }
}
