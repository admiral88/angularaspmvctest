import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { MagazineService } from '../service/magazine.service';
import { MagazineViewModel } from '../models/magazineViewModel';

@Component({
    selector: 'kendo-grid-edit-form-magazine',
    styleUrls: [],
    templateUrl: './editMagazinComponent.html'
})
export class EditMagazineComponent implements OnInit {

    ngOnInit(): void {
    }

    constructor(private magazineService: MagazineService) { }

    public active = false;
    public editForm: FormGroup = new FormGroup({
        'id': new FormControl(),
        'name': new FormControl('', Validators.required),
        'genre': new FormControl('', Validators.required),
        'price': new FormControl('', Validators.compose([Validators.required, Validators.pattern('^[0-9]*')])),
    });

    @Input() public isNew = false;

    @Input() public set model(product: MagazineViewModel) {
        this.editForm.reset(product);
        this.active = product !== undefined;
    }

    @Output() cancel: EventEmitter<any> = new EventEmitter();
    @Output() save: EventEmitter<MagazineViewModel> = new EventEmitter();

    public onSave(e): void {
        e.preventDefault();
        this.save.emit(this.editForm.value);
        this.active = false;
    }


    public onCancel(e): void {
        e.preventDefault();
        this.closeForm();
    }

    private closeForm(): void {
        this.active = false;
    }
}
