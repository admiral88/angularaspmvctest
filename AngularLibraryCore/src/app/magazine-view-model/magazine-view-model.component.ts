import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';

import { State, process } from '@progress/kendo-data-query';
import { GridDataResult } from '@progress/kendo-angular-grid';
import { Observable } from 'rxjs/Observable';
import { MagazineViewModel } from '../models/magazineViewModel';
import { MagazineService } from '../service/magazine.service';
import { UserAuth, UserRoles } from '../models/user';
import { AuthenticationService } from '../service/authentication.service';

@Component({
  selector: 'app-magazine-view-model',
  templateUrl: './magazine-view-model.component.html',
  styleUrls: ['./magazine-view-model.component.css',
    '../../../node_modules/@progress/kendo-theme-default/dist/all.css',
  ],
  encapsulation: ViewEncapsulation.None,
  providers: [MagazineService, AuthenticationService]
})
export class MagazineViewModelComponent implements OnInit {
  public view: Observable<GridDataResult>;
  public gridState: State = {
    sort: [],
    skip: 0,
    take: 10
  };
  public role: boolean = false;
  public userName: string = "not authorized";
  public editDataItem: MagazineViewModel;
  public isNew: boolean;

  constructor(private magazineService: MagazineService, private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.authenticationService.auth().subscribe(result => {
      let user: UserAuth = result;
      if (user.role === UserRoles.admin) {
        this.role = true;
      }
      if (user.userName != null) {
        this.userName = user.userName
      };
    });
    this.view = this.magazineService.map(data => process(data, this.gridState));
    this.magazineService.read();
  }

  public onStateChange(state: State) {
    this.gridState = state;
    this.magazineService.read();
  }

  public addHandler() {
    this.editDataItem = new MagazineViewModel();
    this.isNew = true;
  }

  public editHandler({ dataItem }) {
    this.editDataItem = dataItem;
    this.isNew = false;
  }

  public cancelHandler() {
    this.editDataItem = undefined;
  }

  public saveHandler(product: MagazineViewModel) {
    this.magazineService.save(product, this.isNew);
  }

  public removeHandler({ dataItem }) {
    this.magazineService.remove(dataItem);
  }

}
