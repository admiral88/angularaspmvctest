import { Component, OnInit } from '@angular/core';
import { UserAuth } from './models/user';
import { Router, NavigationEnd } from '@angular/router';
import { AuthenticationService } from './service/authentication.service';

@Component({
  selector: 'app',
  templateUrl: './app.component.html',
  providers: [],
  styleUrls: []
})
export class AppComponent implements OnInit {

  isAuth: boolean = false;
  public userName: string = "not authorized";
  constructor(private authenticationService: AuthenticationService, private router: Router) { }

  ngOnInit() {
    this.router.events
      .subscribe((event) => {

        if (event instanceof NavigationEnd) {
          this.authenticationService.auth().subscribe(result => {
            let user: UserAuth = result;
            this.isAuth = user.isAuth;
            if (user.userName != null) {
              this.userName = user.userName;
            };
          });
        }
      });
  }

  public logout() {
    this.authenticationService.logout().subscribe();
    setTimeout(() => {
      this.isAuth = false;
      this.userName = "not authorized";
    },500);
      
  }

}
