import { Observable } from 'rxjs/Rx';
import { Component, OnInit, Inject, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { GridDataResult } from '@progress/kendo-angular-grid';
import { State, process } from '@progress/kendo-data-query';
import { MagazineService } from '../service/magazine.service';
import { MagazineViewModel } from '../models/magazineViewModel';
import { HomeService } from '../service/home.service';

@Component({
    selector: 'my-app',
    styleUrls: [
      '../../../node_modules/@progress/kendo-theme-default/dist/all.css',

    ],
    encapsulation: ViewEncapsulation.None,

    templateUrl: './home.component.html',
    providers: [HomeService]
})
export class HomeComponent implements OnInit {
    public view: Observable<GridDataResult>;
    public gridState: State = {
        sort: [],
        skip: 0,
        take: 10
    };
    public formGroup: FormGroup;

    private editedRowIndex: number;

    constructor(private mainService: HomeService) { }

    public ngOnInit(): void {
        this.view = this.mainService.map(data => process(data, this.gridState));

        this.mainService.read();
    }

    public onStateChange(state: State) {
        this.gridState = state;

        this.mainService.read();
    }

}
