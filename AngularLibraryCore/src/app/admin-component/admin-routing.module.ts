import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { AdminComponent } from "./admin.component";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "../service/auth-guard.service";


const adminRoutes: Routes = [
      {
        path: '',
        component: AdminComponent,
        canActivate: [AuthGuard]
      }
]


@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(adminRoutes)
  ],
  providers: [],
  exports: [
    RouterModule
  ]
})
export class AdminRoutingModule { }
