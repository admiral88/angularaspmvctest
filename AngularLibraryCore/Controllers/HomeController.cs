using System.Collections.Generic;
using LibraryAngular.BLL.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using LibraryAngular.Views.StoreViewModels;

namespace LibraryAngular.Core.Controllers
{
  [Produces("application/json")]
  [Route("api/home")]
  public class HomeController : BaseController
  {
    StoreService _magazineService;
    public HomeController(IConfiguration configuration):base(configuration)
    {
      _magazineService = new StoreService(connectionString);
    }

    [HttpGet]
    public List<StoreViewModel> Get()
    {
      return _magazineService.GetViewModelStore();
    }
  }
}
