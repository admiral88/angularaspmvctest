using System.Collections.Generic;
using LibraryAngular.BLL.Services;
using LibraryAngular.Views.BookViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace LibraryAngular.Core.Controllers
{
  [Produces("application/json")]
  [Route("api/books")]
  public class BooksController : BaseController
  {
    private BookService _bookService;

    public BooksController(IConfiguration configuration) : base(configuration)
    {
      _bookService = new BookService(connectionString);
    }

    [HttpGet]
    public IEnumerable<BookViewModel> Get()
    {
      
      return _bookService.GetBooks();
    }

    [HttpGet("{id}")]
    public BookViewModel Get(int id)
    {
      BookViewModel book = _bookService.Find(id);
      return book;
    }

    [HttpPost]
    public IActionResult Post([FromBody] BookViewModel bookViewModel)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }
      _bookService.Create(bookViewModel);
      return Ok(bookViewModel);
    }

    [HttpPut("{id}")]
    public IActionResult Put(int id, [FromBody]BookViewModel bookViewModel)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }
      _bookService.Update(bookViewModel);
      return Ok(bookViewModel);
    }

    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
      BookViewModel bookViewModel = _bookService.Find(id);
      if (bookViewModel != null)
      {
        _bookService.Delete(id);
      }
      return Ok(bookViewModel);
    }
  }
}
