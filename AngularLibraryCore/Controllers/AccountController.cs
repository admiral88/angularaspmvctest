using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Newtonsoft.Json;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using LibraryAngular.Entities.Enums;
using LibraryAngular.Views.UserViewModels;
using LibraryAngular.Entities;
using AutoMapper;
using LibraryAngular.Views.EnumViewModels;

namespace LibraryAngular.Core.Controllers
{
  [Produces("application/json")]
  public class AccountController : Controller
  {
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly SignInManager<ApplicationUser> _signInManager;
    RoleManager<IdentityRole> _roleManager;

    public AccountController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, RoleManager<IdentityRole> roleManager)
    {
      _userManager = userManager;
      _signInManager = signInManager;
      _roleManager = roleManager;
    }

    [HttpPost("/token")]
    public async Task Token([FromBody] UserViewModel userRequest)
    {
      ClaimsIdentity identityyhg = new ClaimsIdentity();

      var username = userRequest.UserName;
      var password = userRequest.Password;
      var identity = GetIdentityAsync(username, password);
      var identityResult = identity.Result;
      if (identityResult == null)
      {
        Response.StatusCode = 400;
        await Response.WriteAsync("Invalid username or password.");
        return;
      }

      var now = DateTime.UtcNow;
      var jwt = new JwtSecurityToken(
              issuer: AuthOptions.ISSUER,
              audience: AuthOptions.AUDIENCE,
              notBefore: now,
              claims: identityResult.Claims,
              expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
              signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
      var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

      var response = new
      {
        access_token = encodedJwt,
        username = identityResult.Name
      };
      Response.ContentType = "application/json";
      await Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings { Formatting = Formatting.Indented }));
    }

    private async Task<ClaimsIdentity> GetIdentityAsync(string username, string password)
    {
      var result = await _signInManager.PasswordSignInAsync(username, password, true, false);
      ApplicationUser user = new ApplicationUser();
      if (result.Succeeded)
      {
        user = await _userManager.FindByEmailAsync(username);
        var role = await _userManager.GetRolesAsync(user);
        var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, username),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, role.First())
                };
        ClaimsIdentity claimsIdentity =
        new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
            ClaimsIdentity.DefaultRoleClaimType);
        return claimsIdentity;
      }
      return null;
    }

    [Route("getrole")]
    public UserViewModel GetRole()
    {
      var user = new UserViewModel();
      user.UserName = User.Identity.Name;
      user.IsAuth = User.Identity.IsAuthenticated;
      user.Role = UserRolesViewModel.Guest;
      if (User.IsInRole(UserRolesViewModel.Admin.ToString().ToLower()))
      {
        user.Role = UserRolesViewModel.Admin;
      }
      if (User.IsInRole(UserRolesViewModel.User.ToString().ToLower()))
      {
        user.Role = UserRolesViewModel.User;
      }
      return user;
    }

    [Route("logout")]
    public  void Logout()
    {
       _signInManager.SignOutAsync();
    }
  }
}

