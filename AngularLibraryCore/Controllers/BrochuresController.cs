using System.Collections.Generic;
using LibraryAngular.BLL.Services;
using LibraryAngular.Views.BrochureViewModels;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace LibraryAngular.Core.Controllers
{
  [Produces("application/json")]
  [Route("api/brochures")]
  public class BrochuresController : BaseController
  {
    private BrochureService _brochureService;

    public BrochuresController(IConfiguration configuration):base(configuration)
    {
      _brochureService = new BrochureService(connectionString);
    }


    [HttpGet]
    public List<BrochureViewModel> Get()
    {
      return _brochureService.GetBrochures();
    }

    [HttpGet("{id}")]
    public BrochureViewModel Get(int id)
    {
      var brochure = _brochureService.Find(id);
      return brochure;
    }

    [HttpPost]
    public IActionResult Post([FromBody] BrochureViewModel brochureViewModel)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }
      _brochureService.Create(brochureViewModel);
      return Ok(brochureViewModel);
    }

    [HttpPut("{id}")]
    public IActionResult Put(int id, [FromBody]BrochureViewModel brochureViewModel)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }
      _brochureService.Update(brochureViewModel);
      return Ok(brochureViewModel);
    }

    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
      BrochureViewModel brochureViewModel = _brochureService.Find(id);
      if (brochureViewModel != null)
      {
        _brochureService.Delete(id);
      }
      return Ok(brochureViewModel);
    }
  }
}
