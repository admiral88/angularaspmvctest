using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace LibraryAngular.Core.Controllers
{

  public abstract class BaseController : Controller
  {
    IConfiguration _configuration;
    protected string connectionString;

    public BaseController(IConfiguration configuration)
    {
      _configuration = configuration;
      connectionString = _configuration["ConnectionStrings:DefaultConnection"];
    }
  }
}
