using System.Collections.Generic;
using LibraryAngular.BLL.Services;
using LibraryAngular.Views.MagazineViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace LibraryAngular.Core.Controllers
{
  [Produces("application/json")]
  [Route("api/magazines")]
  public class MagazinesController : BaseController
  {
    private MagazineService _magazineService;

    public MagazinesController(IConfiguration configuration) : base(configuration)
    {
      _magazineService = new MagazineService(connectionString);
    }

    [HttpGet]
    public List<MagazineViewModel> Get()
    {
      return _magazineService.GetMagazines();
    }

    [HttpGet("{id}")]
    public MagazineViewModel Get(int id)
    {
      var magazine = _magazineService.Find(id);
      return magazine;
    }

    [HttpPost]
    public IActionResult Post([FromBody] MagazineViewModel magazineViewModel)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }
      _magazineService.Create(magazineViewModel);
      return Ok(magazineViewModel);
    }

    [HttpPut("{id}")]
    public IActionResult Put(int id, [FromBody] MagazineViewModel magazineViewModel)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }
      _magazineService.Update(magazineViewModel);
      return Ok(magazineViewModel);
    }

    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
      MagazineViewModel magazineViewModel = _magazineService.Find(id);
      if (magazineViewModel != null)
      {
        _magazineService.Delete(id);
      }
      return Ok(magazineViewModel);
    }
  }
}
