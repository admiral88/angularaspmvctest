webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./admin-component/admin.module": [
		"../../../../../src/app/admin-component/admin.module.ts",
		"admin.module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "../../../../../src/app/_directives/alert.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"message\" [ngClass]=\"{ 'alert': message, 'alert-success': message.type === 'success', 'alert-danger': message.type === 'error' }\">{{message.text}}</div>"

/***/ }),

/***/ "../../../../../src/app/_directives/alert.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var alert_service_1 = __webpack_require__("../../../../../src/app/service/alert.service.ts");
var AlertComponent = /** @class */ (function () {
    function AlertComponent(alertService) {
        this.alertService = alertService;
    }
    AlertComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.alertService.getMessage().subscribe(function (message) { _this.message = message; });
    };
    AlertComponent = __decorate([
        core_1.Component({
            selector: 'alert',
            template: __webpack_require__("../../../../../src/app/_directives/alert.component.html")
        }),
        __metadata("design:paramtypes", [alert_service_1.AlertService])
    ], AlertComponent);
    return AlertComponent;
}());
exports.AlertComponent = AlertComponent;


/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"container\">\r\n  <nav style=\"margin-bottom: 20px;\" class=\"navbar navbar-expand-sm bg-primary navbar-dark rounded\">\r\n    <div class=\"container-fluid \">\r\n      <ul class=\"navbar-nav\">\r\n        <li class=\"nav-item\" routerLinkActive=\"active\" [routerLinkActiveOptions]=\"{exact: true}\"><a class=\"nav-link\" routerLink=\"/\">Home</a></li>\r\n        <li class=\"nav-item\" ><a class=\"nav-link\" routerLink=\"/books\">Books</a></li>\r\n        <li class=\"nav-item\" routerLinkActive=\"active\"><a class=\"nav-link\" routerLink=\"/broshures\">Broshures</a></li>\r\n        <li class=\"nav-item\" routerLinkActive=\"active\"><a class=\"nav-link\" routerLink=\"/magazines\">Magazines</a></li>\r\n        <li class=\"nav-item\" routerLinkActive=\"active\"><a class=\"nav-link\" routerLink=\"/admin\">Admin's</a></li>\r\n      </ul>\r\n      <ul class=\"navbar-nav\">\r\n        <li class=\"nav-item navbar-right\"><label class=\"nav-link\">{{userName}}</label> </li>\r\n        <li class=\"nav-item navbar-right\" *ngIf=\"!isAuth\" routerLinkActive=\"active\"><a routerLink=\"/login\" class=\"nav-link\">Login</a></li>\r\n        <li class=\"nav-item navbar-right\" *ngIf=\"isAuth\" routerLinkActive=\"active\"><a routerLink=\"/login\" class=\"nav-link\" (click)=\"logout()\">Logout</a></li>\r\n      </ul>\r\n    </div>\r\n  </nav>\r\n  <div class=\"row\">\r\n    <div class=\"col-xs-10  col-xs-offset-1\">\r\n      <router-outlet></router-outlet>\r\n      <alert></alert>\r\n    </div>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var authentication_service_1 = __webpack_require__("../../../../../src/app/service/authentication.service.ts");
var AppComponent = /** @class */ (function () {
    function AppComponent(authenticationService, router) {
        this.authenticationService = authenticationService;
        this.router = router;
        this.isAuth = false;
        this.userName = "not authorized";
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.router.events
            .subscribe(function (event) {
            if (event instanceof router_1.NavigationEnd) {
                _this.authenticationService.auth().subscribe(function (result) {
                    var user = result;
                    _this.isAuth = user.isAuth;
                    if (user.userName != null) {
                        _this.userName = user.userName;
                    }
                    ;
                });
            }
        });
    };
    AppComponent.prototype.logout = function () {
        var _this = this;
        this.authenticationService.logout().subscribe();
        setTimeout(function () {
            _this.isAuth = false;
            _this.userName = "not authorized";
        }, 500);
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            providers: [],
            styleUrls: []
        }),
        __metadata("design:paramtypes", [authentication_service_1.AuthenticationService, router_1.Router])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;


/***/ }),

/***/ "../../../../../src/app/app.config.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var AppConfig = /** @class */ (function () {
    function AppConfig(document) {
        this.document = document;
        var domain = this.document.location.hostname;
        this.apiUrl = "http://" + domain;
    }
    AppConfig = __decorate([
        __param(0, core_1.Inject(platform_browser_1.DOCUMENT)),
        __metadata("design:paramtypes", [Object])
    ], AppConfig);
    return AppConfig;
}());
exports.AppConfig = AppConfig;
;


/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var platform_browser_1 = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var animations_1 = __webpack_require__("../../../platform-browser/esm5/animations.js");
var forms_1 = __webpack_require__("../../../forms/esm5/forms.js");
var app_component_1 = __webpack_require__("../../../../../src/app/app.component.ts");
var http_1 = __webpack_require__("../../../http/esm5/http.js");
var http_2 = __webpack_require__("../../../common/esm5/http.js");
var book_view_model_component_1 = __webpack_require__("../../../../../src/app/book-view-model/book-view-model.component.ts");
var brochure_view_model_component_1 = __webpack_require__("../../../../../src/app/brochure-view-model/brochure-view-model.component.ts");
var magazine_view_model_component_1 = __webpack_require__("../../../../../src/app/magazine-view-model/magazine-view-model.component.ts");
var kendo_angular_grid_1 = __webpack_require__("../../../../@progress/kendo-angular-grid/dist/es/main.js");
var kendo_angular_buttons_1 = __webpack_require__("../../../../@progress/kendo-angular-buttons/dist/es/main.js");
var editBrochureComponent_component_1 = __webpack_require__("../../../../../src/app/brochure-view-model/editBrochureComponent.component.ts");
var kendo_angular_dialog_1 = __webpack_require__("../../../../@progress/kendo-angular-dialog/dist/es/main.js");
var editMagazineComponent_component_1 = __webpack_require__("../../../../../src/app/magazine-view-model/editMagazineComponent.component.ts");
var editBookComponent_component_1 = __webpack_require__("../../../../../src/app/book-view-model/editBookComponent.component.ts");
var home_component_1 = __webpack_require__("../../../../../src/app/home-component/home.component.ts");
var login_component_1 = __webpack_require__("../../../../../src/app/login-component/login.component.ts");
var authentication_service_1 = __webpack_require__("../../../../../src/app/service/authentication.service.ts");
var alert_service_1 = __webpack_require__("../../../../../src/app/service/alert.service.ts");
var alert_component_1 = __webpack_require__("../../../../../src/app/_directives/alert.component.ts");
var app_config_1 = __webpack_require__("../../../../../src/app/app.config.ts");
var auth_guard_service_1 = __webpack_require__("../../../../../src/app/service/auth-guard.service.ts");
var appRoutes = [
    {
        path: '',
        component: home_component_1.HomeComponent
    },
    {
        path: 'books',
        component: book_view_model_component_1.BookViewModelComponent
    },
    {
        path: 'broshures',
        component: brochure_view_model_component_1.BrochureViewModelComponent
    },
    {
        path: 'magazines',
        component: magazine_view_model_component_1.MagazineViewModelComponent
    },
    {
        path: 'login',
        component: login_component_1.LoginComponent
    },
    {
        path: 'logout',
        component: login_component_1.LoginComponent
    },
    {
        path: 'admin',
        loadChildren: './admin-component/admin.module#AdminModule'
    }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                forms_1.ReactiveFormsModule,
                platform_browser_1.BrowserModule,
                animations_1.BrowserAnimationsModule,
                forms_1.FormsModule,
                http_2.HttpClientModule,
                http_1.HttpModule,
                router_1.RouterModule.forRoot(appRoutes),
                kendo_angular_grid_1.GridModule,
                kendo_angular_buttons_1.ButtonsModule,
                kendo_angular_dialog_1.DialogModule,
            ],
            declarations: [
                app_component_1.AppComponent,
                book_view_model_component_1.BookViewModelComponent,
                brochure_view_model_component_1.BrochureViewModelComponent,
                magazine_view_model_component_1.MagazineViewModelComponent,
                editBrochureComponent_component_1.EditBrochureComponent,
                editMagazineComponent_component_1.EditMagazineComponent,
                editBookComponent_component_1.EditBookComponent,
                home_component_1.HomeComponent,
                login_component_1.LoginComponent,
                alert_component_1.AlertComponent,
            ],
            bootstrap: [app_component_1.AppComponent],
            providers: [
                app_config_1.AppConfig,
                auth_guard_service_1.AuthGuard,
                alert_service_1.AlertService,
                authentication_service_1.AuthenticationService,
            ]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;


/***/ }),

/***/ "../../../../../src/app/book-view-model/book-view-model.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/book-view-model/book-view-model.component.html":
/***/ (function(module, exports) {

module.exports = "<kendo-grid [data]=\"view | async\"\r\n            [height]=\"533\"\r\n            [pageSize]=\"gridState.take\" [skip]=\"gridState.skip\" [sort]=\"gridState.sort\"\r\n            [pageable]=\"true\" [sortable]=\"true\"\r\n            (dataStateChange)=\"onStateChange($event)\"\r\n            (edit)=\"editHandler($event)\" (remove)=\"removeHandler($event)\"\r\n            (add)=\"addHandler($event)\">\r\n  <ng-template kendoGridToolbarTemplate>\r\n    <button kendoGridAddCommand *ngIf=\"role\">Add new</button>\r\n    <label>{{userName}}</label>\r\n  </ng-template>\r\n  <kendo-grid-column field=\"id\" title=\"ID\" width=\"100\">\r\n  </kendo-grid-column>\r\n  <kendo-grid-column field=\"name\" title=\"Name\" width=\"120\">\r\n  </kendo-grid-column>\r\n  <kendo-grid-column field=\"author\" title=\"Author\" width=\"120\">\r\n  </kendo-grid-column>\r\n  <kendo-grid-column field=\"dateFrom\" title=\"Date From\" width=\"120\">\r\n    <ng-template kendoGridCellTemplate let-dataItem>\r\n      {{ dataItem.dateFrom | date:'dd.MM.yyyy HH:mm' }}\r\n    </ng-template>\r\n  </kendo-grid-column>\r\n  <kendo-grid-column field=\"price\" title=\"Price\" width=\"100\" format=\"{0:c}\">\r\n  </kendo-grid-column>\r\n  <kendo-grid-column field=\"genre\" title=\"genre\" width=\"100\">\r\n  </kendo-grid-column>\r\n  <kendo-grid-command-column title=\"command\" width=\"220\">\r\n    <ng-template kendoGridCellTemplate let-isNew=\"isNew\">\r\n      <button kendoGridEditCommand *ngIf=\"role\" class=\"k-primary\">Edit</button>\r\n      <button kendoGridRemoveCommand *ngIf=\"role\">Remove</button>\r\n      <button kendoGridSaveCommand [disabled]=\"formGroup?.invalid\">{{ isNew ? 'Add' : 'Update' }}</button>\r\n      <button kendoGridCancelCommand>{{ isNew ? 'Discard changes' : 'Cancel' }}</button>\r\n    </ng-template>\r\n  </kendo-grid-command-column>\r\n</kendo-grid>\r\n\r\n<kendo-grid-edit-form-book [model]=\"editDataItem\" [isNew]=\"isNew\"\r\n                      (save)=\"saveHandler($event)\"\r\n                      (cancel)=\"cancelHandler()\">\r\n</kendo-grid-edit-form-book>\r\n"

/***/ }),

/***/ "../../../../../src/app/book-view-model/book-view-model.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var bookViewModel_1 = __webpack_require__("../../../../../src/app/models/bookViewModel.ts");
var book_service_1 = __webpack_require__("../../../../../src/app/service/book.service.ts");
var user_1 = __webpack_require__("../../../../../src/app/models/user.ts");
var kendo_data_query_1 = __webpack_require__("../../../../@progress/kendo-data-query/dist/es/main.js");
var authentication_service_1 = __webpack_require__("../../../../../src/app/service/authentication.service.ts");
var BookViewModelComponent = /** @class */ (function () {
    function BookViewModelComponent(bookService, authenticationService) {
        this.bookService = bookService;
        this.authenticationService = authenticationService;
        this.role = false;
        this.userName = "not authorized";
        this.gridState = {
            sort: [],
            skip: 0,
            take: 10
        };
    }
    BookViewModelComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authenticationService.auth().subscribe(function (result) {
            var user = result;
            if (user.role === user_1.UserRoles.admin) {
                _this.role = true;
            }
            if (user.userName != null) {
                _this.userName = user.userName;
            }
            ;
        });
        this.view = this.bookService.map(function (data) { return kendo_data_query_1.process(data, _this.gridState); });
        this.bookService.read();
    };
    BookViewModelComponent.prototype.onStateChange = function (state) {
        this.gridState = state;
        this.bookService.read();
    };
    BookViewModelComponent.prototype.addHandler = function () {
        this.editDataItem = new bookViewModel_1.BookViewModel();
        this.isNew = true;
    };
    BookViewModelComponent.prototype.editHandler = function (_a) {
        var dataItem = _a.dataItem;
        this.editDataItem = dataItem;
        this.isNew = false;
    };
    BookViewModelComponent.prototype.cancelHandler = function () {
        this.editDataItem = undefined;
    };
    BookViewModelComponent.prototype.saveHandler = function (product) {
        this.bookService.save(product, this.isNew);
    };
    BookViewModelComponent.prototype.removeHandler = function (_a) {
        var dataItem = _a.dataItem;
        this.bookService.remove(dataItem);
    };
    BookViewModelComponent = __decorate([
        core_1.Component({
            selector: 'app-book-view-model',
            template: __webpack_require__("../../../../../src/app/book-view-model/book-view-model.component.html"),
            styles: [__webpack_require__("../../../../../src/app/book-view-model/book-view-model.component.css"), __webpack_require__("../../../../@progress/kendo-theme-default/dist/all.css")],
            encapsulation: core_1.ViewEncapsulation.None,
            providers: [book_service_1.BookService, authentication_service_1.AuthenticationService]
        }),
        __metadata("design:paramtypes", [book_service_1.BookService, authentication_service_1.AuthenticationService])
    ], BookViewModelComponent);
    return BookViewModelComponent;
}());
exports.BookViewModelComponent = BookViewModelComponent;


/***/ }),

/***/ "../../../../../src/app/book-view-model/editBookComponent.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var forms_1 = __webpack_require__("../../../forms/esm5/forms.js");
var book_service_1 = __webpack_require__("../../../../../src/app/service/book.service.ts");
var bookViewModel_1 = __webpack_require__("../../../../../src/app/models/bookViewModel.ts");
var EditBookComponent = /** @class */ (function () {
    function EditBookComponent(brochureService) {
        this.brochureService = brochureService;
        this.active = false;
        this.editForm = new forms_1.FormGroup({
            'id': new forms_1.FormControl(),
            'name': new forms_1.FormControl('', forms_1.Validators.required),
            'author': new forms_1.FormControl('', forms_1.Validators.required),
            'genre': new forms_1.FormControl('', forms_1.Validators.required),
            'price': new forms_1.FormControl('', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.pattern('^[0-9]*')])),
        });
        this.isNew = false;
        this.cancel = new core_1.EventEmitter();
        this.save = new core_1.EventEmitter();
    }
    EditBookComponent.prototype.ngOnInit = function () {
    };
    Object.defineProperty(EditBookComponent.prototype, "model", {
        set: function (product) {
            this.editForm.reset(product);
            this.active = product !== undefined;
        },
        enumerable: true,
        configurable: true
    });
    EditBookComponent.prototype.onSave = function (e) {
        e.preventDefault();
        this.save.emit(this.editForm.value);
        this.active = false;
    };
    EditBookComponent.prototype.onCancel = function (e) {
        e.preventDefault();
        this.closeForm();
    };
    EditBookComponent.prototype.closeForm = function () {
        this.active = false;
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], EditBookComponent.prototype, "isNew", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", bookViewModel_1.BookViewModel),
        __metadata("design:paramtypes", [bookViewModel_1.BookViewModel])
    ], EditBookComponent.prototype, "model", null);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], EditBookComponent.prototype, "cancel", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], EditBookComponent.prototype, "save", void 0);
    EditBookComponent = __decorate([
        core_1.Component({
            selector: 'kendo-grid-edit-form-book',
            styleUrls: [],
            template: __webpack_require__("../../../../../src/app/book-view-model/editBookComponent.html")
        }),
        __metadata("design:paramtypes", [book_service_1.BookService])
    ], EditBookComponent);
    return EditBookComponent;
}());
exports.EditBookComponent = EditBookComponent;


/***/ }),

/***/ "../../../../../src/app/book-view-model/editBookComponent.html":
/***/ (function(module, exports) {

module.exports = "<kendo-dialog *ngIf=\"active\" (close)=\"closeForm()\">\r\n    <kendo-dialog-titlebar>\r\n        {{ isNew ? 'Add new product' : 'Edit product' }}\r\n    </kendo-dialog-titlebar>\r\n\r\n    <form novalidate [formGroup]=\"editForm\">\r\n      <div class=\"form-group\">\r\n        <label for=\"name\" class=\"control-label\">Name</label>\r\n\r\n        <input type=\"text\" class=\"k-textbox\" formControlName=\"name\" />\r\n\r\n        <div class=\"k-tooltip k-tooltip-validation\"\r\n             [hidden]=\"editForm.controls.name.valid || editForm.controls.name.pristine\">\r\n          ProductName is required\r\n        </div>\r\n      </div>\r\n      <div class=\"form-group\">\r\n        <label for=\"name\" class=\"control-label\">Author</label>\r\n\r\n        <input type=\"text\" class=\"k-textbox\" formControlName=\"author\" />\r\n\r\n        <div class=\"k-tooltip k-tooltip-validation\"\r\n             [hidden]=\"editForm.controls.author.valid || editForm.controls.author.pristine\">\r\n          Author is required\r\n        </div>\r\n      </div>\r\n      <div class=\"form-group\">\r\n        <label for=\"genre\" class=\"control-label\">Genre</label>\r\n\r\n        <input type=\"text\" class=\"k-textbox\" formControlName=\"genre\" />\r\n        <div class=\"k-tooltip k-tooltip-validation\"\r\n             [hidden]=\"editForm.controls.genre.valid || editForm.controls.genre.pristine\">\r\n          Genre is required\r\n        </div>\r\n      </div>\r\n      <div class=\"form-group\">\r\n        <label for=\"price\" class=\"control-label\">Price</label>\r\n\r\n        <input type=\"text\" class=\"k-textbox\" formControlName=\"price\" />\r\n\r\n        <div class=\"k-tooltip k-tooltip-validation\"\r\n             [hidden]=\"editForm.controls.price.valid || editForm.controls.price.pristine\">\r\n          Price must be number\r\n        </div>\r\n      </div>\r\n    </form>\r\n\r\n    <kendo-dialog-actions>\r\n        <button class=\"k-button\" (click)=\"onCancel($event)\">Cancel</button>\r\n        <button class=\"k-button k-primary\" [disabled]=\"!editForm.valid\" (click)=\"onSave($event)\">Save</button>\r\n    </kendo-dialog-actions>\r\n</kendo-dialog>\r\n"

/***/ }),

/***/ "../../../../../src/app/brochure-view-model/brochure-view-model.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/brochure-view-model/brochure-view-model.component.html":
/***/ (function(module, exports) {

module.exports = "<kendo-grid [data]=\"view | async\"\r\n            [height]=\"533\"\r\n            [pageSize]=\"gridState.take\" [skip]=\"gridState.skip\" [sort]=\"gridState.sort\"\r\n            [pageable]=\"true\" [sortable]=\"true\"\r\n            (dataStateChange)=\"onStateChange($event)\"\r\n            (edit)=\"editHandler($event)\" (remove)=\"removeHandler($event)\"\r\n            (add)=\"addHandler($event)\">\r\n    <ng-template kendoGridToolbarTemplate>\r\n        <button kendoGridAddCommand *ngIf=\"role\">Add new</button>\r\n        <label>{{userName}}</label>\r\n    </ng-template>\r\n    <kendo-grid-column field=\"id\" title=\"ID\" width=\"100\">\r\n    </kendo-grid-column>\r\n    <kendo-grid-column field=\"name\" title=\"Name\" width=\"120\">\r\n\r\n    </kendo-grid-column>\r\n    <kendo-grid-column field=\"dateFrom\" title=\"Date From\" width=\"120\">\r\n        <ng-template kendoGridCellTemplate let-dataItem>\r\n            {{ dataItem.dateFrom | date:'dd.MM.yyyy HH:mm' }}\r\n        </ng-template>\r\n    </kendo-grid-column>\r\n    <kendo-grid-column field=\"price\" title=\"Price\" width=\"100\" format=\"{0:c}\">\r\n    </kendo-grid-column>\r\n    <kendo-grid-column field=\"genre\" title=\"genre\" width=\"100\">\r\n    </kendo-grid-column>\r\n    <kendo-grid-command-column title=\"command\" width=\"220\">\r\n        <ng-template kendoGridCellTemplate let-isNew=\"isNew\">\r\n            <button kendoGridEditCommand class=\"k-primary\" *ngIf=\"role\">Edit</button>\r\n            <button kendoGridRemoveCommand *ngIf=\"role\">Remove</button>\r\n            <button kendoGridSaveCommand [disabled]=\"formGroup?.invalid\">{{ isNew ? 'Add' : 'Update' }}</button>\r\n            <button kendoGridCancelCommand>{{ isNew ? 'Discard changes' : 'Cancel' }}</button>\r\n        </ng-template>\r\n    </kendo-grid-command-column>\r\n</kendo-grid>\r\n\r\n<kendo-grid-edit-form [model]=\"editDataItem\" [isNew]=\"isNew\"\r\n                      (save)=\"saveHandler($event)\"\r\n                      (cancel)=\"cancelHandler()\">\r\n</kendo-grid-edit-form>\r\n"

/***/ }),

/***/ "../../../../../src/app/brochure-view-model/brochure-view-model.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var kendo_data_query_1 = __webpack_require__("../../../../@progress/kendo-data-query/dist/es/main.js");
var brochureViewModel_1 = __webpack_require__("../../../../../src/app/models/brochureViewModel.ts");
var brochure_service_1 = __webpack_require__("../../../../../src/app/service/brochure.service.ts");
var user_1 = __webpack_require__("../../../../../src/app/models/user.ts");
var authentication_service_1 = __webpack_require__("../../../../../src/app/service/authentication.service.ts");
var BrochureViewModelComponent = /** @class */ (function () {
    function BrochureViewModelComponent(brochureService, authenticationService) {
        this.brochureService = brochureService;
        this.authenticationService = authenticationService;
        this.role = false;
        this.userName = "not authorized";
        this.gridState = {
            sort: [],
            skip: 0,
            take: 10
        };
    }
    BrochureViewModelComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authenticationService.auth().subscribe(function (result) {
            var user = result;
            if (user.role === user_1.UserRoles.admin) {
                _this.role = true;
            }
            if (user.userName != null) {
                _this.userName = user.userName;
            }
            ;
        });
        this.view = this.brochureService.map(function (data) { return kendo_data_query_1.process(data, _this.gridState); });
        this.brochureService.read();
    };
    BrochureViewModelComponent.prototype.onStateChange = function (state) {
        this.gridState = state;
        this.brochureService.read();
    };
    BrochureViewModelComponent.prototype.addHandler = function () {
        this.editDataItem = new brochureViewModel_1.BrochureViewModel();
        this.isNew = true;
    };
    BrochureViewModelComponent.prototype.editHandler = function (_a) {
        var dataItem = _a.dataItem;
        this.editDataItem = dataItem;
        this.isNew = false;
    };
    BrochureViewModelComponent.prototype.cancelHandler = function () {
        this.editDataItem = undefined;
    };
    BrochureViewModelComponent.prototype.saveHandler = function (product) {
        this.brochureService.save(product, this.isNew);
    };
    BrochureViewModelComponent.prototype.removeHandler = function (_a) {
        var dataItem = _a.dataItem;
        this.brochureService.remove(dataItem);
    };
    BrochureViewModelComponent = __decorate([
        core_1.Component({
            selector: 'app-brochure-view-model',
            template: __webpack_require__("../../../../../src/app/brochure-view-model/brochure-view-model.component.html"),
            styles: [__webpack_require__("../../../../../src/app/brochure-view-model/brochure-view-model.component.css"), __webpack_require__("../../../../@progress/kendo-theme-default/dist/all.css")],
            encapsulation: core_1.ViewEncapsulation.None,
            providers: [brochure_service_1.BrochureService, authentication_service_1.AuthenticationService]
        }),
        __metadata("design:paramtypes", [brochure_service_1.BrochureService, authentication_service_1.AuthenticationService])
    ], BrochureViewModelComponent);
    return BrochureViewModelComponent;
}());
exports.BrochureViewModelComponent = BrochureViewModelComponent;


/***/ }),

/***/ "../../../../../src/app/brochure-view-model/editBrochureComponent.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var forms_1 = __webpack_require__("../../../forms/esm5/forms.js");
var brochureViewModel_1 = __webpack_require__("../../../../../src/app/models/brochureViewModel.ts");
var brochure_service_1 = __webpack_require__("../../../../../src/app/service/brochure.service.ts");
var EditBrochureComponent = /** @class */ (function () {
    function EditBrochureComponent(brochureService) {
        this.brochureService = brochureService;
        this.active = false;
        this.editForm = new forms_1.FormGroup({
            'id': new forms_1.FormControl(),
            'name': new forms_1.FormControl('', forms_1.Validators.required),
            'genre': new forms_1.FormControl('', forms_1.Validators.required),
            'price': new forms_1.FormControl('', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.pattern('^[0-9]*')])),
        });
        this.isNew = false;
        this.cancel = new core_1.EventEmitter();
        this.save = new core_1.EventEmitter();
    }
    EditBrochureComponent.prototype.ngOnInit = function () {
    };
    Object.defineProperty(EditBrochureComponent.prototype, "model", {
        set: function (product) {
            this.editForm.reset(product);
            this.active = product !== undefined;
        },
        enumerable: true,
        configurable: true
    });
    EditBrochureComponent.prototype.onSave = function (e) {
        e.preventDefault();
        this.save.emit(this.editForm.value);
        this.active = false;
    };
    EditBrochureComponent.prototype.onCancel = function (e) {
        e.preventDefault();
        this.closeForm();
    };
    EditBrochureComponent.prototype.closeForm = function () {
        this.active = false;
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], EditBrochureComponent.prototype, "isNew", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", brochureViewModel_1.BrochureViewModel),
        __metadata("design:paramtypes", [brochureViewModel_1.BrochureViewModel])
    ], EditBrochureComponent.prototype, "model", null);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], EditBrochureComponent.prototype, "cancel", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], EditBrochureComponent.prototype, "save", void 0);
    EditBrochureComponent = __decorate([
        core_1.Component({
            selector: 'kendo-grid-edit-form',
            styleUrls: [],
            template: __webpack_require__("../../../../../src/app/brochure-view-model/editBrochureComponent.html")
        }),
        __metadata("design:paramtypes", [brochure_service_1.BrochureService])
    ], EditBrochureComponent);
    return EditBrochureComponent;
}());
exports.EditBrochureComponent = EditBrochureComponent;


/***/ }),

/***/ "../../../../../src/app/brochure-view-model/editBrochureComponent.html":
/***/ (function(module, exports) {

module.exports = "<kendo-dialog *ngIf=\"active\" (close)=\"closeForm()\">\r\n    <kendo-dialog-titlebar>\r\n        {{ isNew ? 'Add new product' : 'Edit product' }}\r\n    </kendo-dialog-titlebar>\r\n\r\n    <form novalidate [formGroup]=\"editForm\">\r\n        <div class=\"form-group\">\r\n            <label for=\"name\" class=\"control-label\">Name</label>\r\n\r\n            <input type=\"text\" class=\"k-textbox\" formControlName=\"name\" />\r\n\r\n            <div class=\"k-tooltip k-tooltip-validation\"\r\n                 [hidden]=\"editForm.controls.name.valid || editForm.controls.name.pristine\">\r\n                ProductName is required\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label for=\"genre\" class=\"control-label\">Genre</label>\r\n\r\n            <input type=\"text\" class=\"k-textbox\" formControlName=\"genre\" />\r\n            <div class=\"k-tooltip k-tooltip-validation\"\r\n                 [hidden]=\"editForm.controls.genre.valid || editForm.controls.genre.pristine\">\r\n                Genre is required\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label for=\"price\" class=\"control-label\">Price</label>\r\n\r\n            <input type=\"text\" class=\"k-textbox\" formControlName=\"price\" />\r\n\r\n            <div class=\"k-tooltip k-tooltip-validation\"\r\n                 [hidden]=\"editForm.controls.price.valid || editForm.controls.price.pristine\">\r\n                Price must be number\r\n            </div>\r\n        </div>\r\n    </form>\r\n\r\n    <kendo-dialog-actions>\r\n        <button class=\"k-button\" (click)=\"onCancel($event)\">Cancel</button>\r\n        <button class=\"k-button k-primary\" [disabled]=\"!editForm.valid\" (click)=\"onSave($event)\">Save</button>\r\n    </kendo-dialog-actions>\r\n</kendo-dialog>"

/***/ }),

/***/ "../../../../../src/app/home-component/home.component.html":
/***/ (function(module, exports) {

module.exports = "<kendo-grid [data]=\"view | async\"\r\n            [height]=\"533\"\r\n            [pageSize]=\"gridState.take\" [skip]=\"gridState.skip\" [sort]=\"gridState.sort\"\r\n            [pageable]=\"true\" [sortable]=\"true\"\r\n            (dataStateChange)=\"onStateChange($event)\"\r\n>\r\n    <kendo-grid-column field=\"name\" title=\"Product Name\"></kendo-grid-column>\r\n    <kendo-grid-column field=\"dateFrom\" title=\"Date From\">\r\n        <ng-template kendoGridCellTemplate let-dataItem>\r\n            {{ dataItem.dateFrom | date:'dd.MM.yyyy HH:mm' }}\r\n        </ng-template>\r\n    </kendo-grid-column>\r\n    <kendo-grid-column field=\"theme\" title=\"Theme\"></kendo-grid-column>\r\n    <kendo-grid-column field=\"price\" title=\"Price\" format=\"{0:c}\"></kendo-grid-column>\r\n    <kendo-grid-column field=\"typeProduct\" title=\"Type Product\"></kendo-grid-column>\r\n</kendo-grid>\r\n"

/***/ }),

/***/ "../../../../../src/app/home-component/home.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var kendo_data_query_1 = __webpack_require__("../../../../@progress/kendo-data-query/dist/es/main.js");
var home_service_1 = __webpack_require__("../../../../../src/app/service/home.service.ts");
var HomeComponent = /** @class */ (function () {
    function HomeComponent(mainService) {
        this.mainService = mainService;
        this.gridState = {
            sort: [],
            skip: 0,
            take: 10
        };
    }
    HomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.view = this.mainService.map(function (data) { return kendo_data_query_1.process(data, _this.gridState); });
        this.mainService.read();
    };
    HomeComponent.prototype.onStateChange = function (state) {
        this.gridState = state;
        this.mainService.read();
    };
    HomeComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            styles: [__webpack_require__("../../../../@progress/kendo-theme-default/dist/all.css")],
            encapsulation: core_1.ViewEncapsulation.None,
            template: __webpack_require__("../../../../../src/app/home-component/home.component.html"),
            providers: [home_service_1.HomeService]
        }),
        __metadata("design:paramtypes", [home_service_1.HomeService])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;


/***/ }),

/***/ "../../../../../src/app/login-component/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"col\">\r\n    <h2>Login</h2>\r\n    <form name=\"form\" (ngSubmit)=\"f.form.valid && login()\" #f=\"ngForm\" novalidate>\r\n        <div class=\"form-group\" [ngClass]=\"{ 'has-error': f.submitted && !username.valid }\">\r\n            <label for=\"username\">Username</label>\r\n            <input type=\"email\" class=\"form-control\" name=\"username\" [(ngModel)]=\"model.username\" #username=\"ngModel\" required />\r\n            <div *ngIf=\"f.submitted && !username.valid\" class=\"help-block\">Username is required</div>\r\n        </div>\r\n        <div class=\"form-group\" [ngClass]=\"{ 'has-error': f.submitted && !password.valid }\">\r\n            <label for=\"password\">Password</label>\r\n            <input type=\"password\" class=\"form-control\" name=\"password\" [(ngModel)]=\"model.password\" #password=\"ngModel\" required />\r\n            <div *ngIf=\"f.submitted && !password.valid\" class=\"help-block\">Password is required</div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <button [disabled]=\"loading\" class=\"btn btn-primary\">Login</button>\r\n            <img *ngIf=\"loading\" src=\"data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==\" />\r\n        </div>\r\n</form>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/login-component/login.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var authentication_service_1 = __webpack_require__("../../../../../src/app/service/authentication.service.ts");
var alert_service_1 = __webpack_require__("../../../../../src/app/service/alert.service.ts");
var LoginComponent = /** @class */ (function () {
    function LoginComponent(route, router, authenticationService, alertService) {
        this.route = route;
        this.router = router;
        this.authenticationService = authenticationService;
        this.alertService = alertService;
        this.model = {};
        this.loading = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.authenticationService.logout();
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.loading = true;
        this.authenticationService.login(this.model.username, this.model.password)
            .subscribe(function (data) {
            _this.router.navigate([_this.returnUrl]);
        }, function (error) {
            _this.alertService.error(error._body);
            _this.loading = false;
        });
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'login-app',
            template: __webpack_require__("../../../../../src/app/login-component/login.component.html")
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            router_1.Router,
            authentication_service_1.AuthenticationService,
            alert_service_1.AlertService])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;


/***/ }),

/***/ "../../../../../src/app/magazine-view-model/editMagazinComponent.html":
/***/ (function(module, exports) {

module.exports = "<kendo-dialog *ngIf=\"active\" (close)=\"closeForm()\">\r\n    <kendo-dialog-titlebar>\r\n        {{ isNew ? 'Add new product' : 'Edit product' }}\r\n    </kendo-dialog-titlebar>\r\n\r\n    <form novalidate [formGroup]=\"editForm\">\r\n        <div class=\"form-group\">\r\n            <label for=\"name\" class=\"control-label\">Name</label>\r\n\r\n            <input type=\"text\" class=\"k-textbox\" formControlName=\"name\" />\r\n\r\n            <div class=\"k-tooltip k-tooltip-validation\"\r\n                 [hidden]=\"editForm.controls.name.valid || editForm.controls.name.pristine\">\r\n                ProductName is required\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label for=\"genre\" class=\"control-label\">Genre</label>\r\n\r\n            <input type=\"text\" class=\"k-textbox\" formControlName=\"genre\" />\r\n            <div class=\"k-tooltip k-tooltip-validation\"\r\n                 [hidden]=\"editForm.controls.genre.valid || editForm.controls.genre.pristine\">\r\n                Genre is required\r\n            </div>\r\n        </div>\r\n        <div class=\"form-group\">\r\n            <label for=\"price\" class=\"control-label\">Price</label>\r\n\r\n            <input type=\"text\" class=\"k-textbox\" formControlName=\"price\" />\r\n\r\n            <div class=\"k-tooltip k-tooltip-validation\"\r\n                 [hidden]=\"editForm.controls.price.valid || editForm.controls.price.pristine\">\r\n                Price must be number\r\n            </div>\r\n        </div>\r\n    </form>\r\n\r\n    <kendo-dialog-actions>\r\n        <button class=\"k-button\" (click)=\"onCancel($event)\">Cancel</button>\r\n        <button class=\"k-button k-primary\" [disabled]=\"!editForm.valid\" (click)=\"onSave($event)\">Save</button>\r\n    </kendo-dialog-actions>\r\n</kendo-dialog>"

/***/ }),

/***/ "../../../../../src/app/magazine-view-model/editMagazineComponent.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var forms_1 = __webpack_require__("../../../forms/esm5/forms.js");
var magazine_service_1 = __webpack_require__("../../../../../src/app/service/magazine.service.ts");
var magazineViewModel_1 = __webpack_require__("../../../../../src/app/models/magazineViewModel.ts");
var EditMagazineComponent = /** @class */ (function () {
    function EditMagazineComponent(magazineService) {
        this.magazineService = magazineService;
        this.active = false;
        this.editForm = new forms_1.FormGroup({
            'id': new forms_1.FormControl(),
            'name': new forms_1.FormControl('', forms_1.Validators.required),
            'genre': new forms_1.FormControl('', forms_1.Validators.required),
            'price': new forms_1.FormControl('', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.pattern('^[0-9]*')])),
        });
        this.isNew = false;
        this.cancel = new core_1.EventEmitter();
        this.save = new core_1.EventEmitter();
    }
    EditMagazineComponent.prototype.ngOnInit = function () {
    };
    Object.defineProperty(EditMagazineComponent.prototype, "model", {
        set: function (product) {
            this.editForm.reset(product);
            this.active = product !== undefined;
        },
        enumerable: true,
        configurable: true
    });
    EditMagazineComponent.prototype.onSave = function (e) {
        e.preventDefault();
        this.save.emit(this.editForm.value);
        this.active = false;
    };
    EditMagazineComponent.prototype.onCancel = function (e) {
        e.preventDefault();
        this.closeForm();
    };
    EditMagazineComponent.prototype.closeForm = function () {
        this.active = false;
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], EditMagazineComponent.prototype, "isNew", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", magazineViewModel_1.MagazineViewModel),
        __metadata("design:paramtypes", [magazineViewModel_1.MagazineViewModel])
    ], EditMagazineComponent.prototype, "model", null);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], EditMagazineComponent.prototype, "cancel", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", core_1.EventEmitter)
    ], EditMagazineComponent.prototype, "save", void 0);
    EditMagazineComponent = __decorate([
        core_1.Component({
            selector: 'kendo-grid-edit-form-magazine',
            styleUrls: [],
            template: __webpack_require__("../../../../../src/app/magazine-view-model/editMagazinComponent.html")
        }),
        __metadata("design:paramtypes", [magazine_service_1.MagazineService])
    ], EditMagazineComponent);
    return EditMagazineComponent;
}());
exports.EditMagazineComponent = EditMagazineComponent;


/***/ }),

/***/ "../../../../../src/app/magazine-view-model/magazine-view-model.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/magazine-view-model/magazine-view-model.component.html":
/***/ (function(module, exports) {

module.exports = "<kendo-grid [data]=\"view | async\"\r\n            [height]=\"533\"\r\n            [pageSize]=\"gridState.take\" [skip]=\"gridState.skip\" [sort]=\"gridState.sort\"\r\n            [pageable]=\"true\" [sortable]=\"true\"\r\n            (dataStateChange)=\"onStateChange($event)\"\r\n            (edit)=\"editHandler($event)\" (remove)=\"removeHandler($event)\"\r\n            (add)=\"addHandler($event)\">\r\n    <ng-template kendoGridToolbarTemplate>\r\n        <button kendoGridAddCommand *ngIf=\"role\">Add new</button>\r\n        <label>{{userName}}</label>\r\n    </ng-template>\r\n    <kendo-grid-column field=\"id\" title=\"ID\" width=\"100\">\r\n    </kendo-grid-column>\r\n    <kendo-grid-column field=\"name\" title=\"Name\" width=\"120\">\r\n\r\n    </kendo-grid-column>\r\n    <kendo-grid-column field=\"dateFrom\" title=\"Date From\" width=\"120\">\r\n        <ng-template kendoGridCellTemplate let-dataItem>\r\n            {{ dataItem.dateFrom | date:'dd.MM.yyyy HH:mm' }}\r\n        </ng-template>\r\n    </kendo-grid-column>\r\n    <kendo-grid-column field=\"price\" title=\"Price\" width=\"100\" format=\"{0:c}\">\r\n    </kendo-grid-column>\r\n    <kendo-grid-column field=\"genre\" title=\"genre\" width=\"100\">\r\n    </kendo-grid-column>\r\n    <kendo-grid-command-column title=\"command\" width=\"220\">\r\n        <ng-template kendoGridCellTemplate let-isNew=\"isNew\">\r\n            <button kendoGridEditCommand class=\"k-primary\" *ngIf=\"role\">Edit</button>\r\n            <button kendoGridRemoveCommand *ngIf=\"role\">Remove</button>\r\n            <button kendoGridSaveCommand [disabled]=\"formGroup?.invalid\">{{ isNew ? 'Add' : 'Update' }}</button>\r\n            <button kendoGridCancelCommand>{{ isNew ? 'Discard changes' : 'Cancel' }}</button>\r\n        </ng-template>\r\n    </kendo-grid-command-column>\r\n</kendo-grid>\r\n\r\n<kendo-grid-edit-form-magazine [model]=\"editDataItem\" [isNew]=\"isNew\"\r\n                      (save)=\"saveHandler($event)\"\r\n                      (cancel)=\"cancelHandler()\">\r\n</kendo-grid-edit-form-magazine>\r\n"

/***/ }),

/***/ "../../../../../src/app/magazine-view-model/magazine-view-model.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var kendo_data_query_1 = __webpack_require__("../../../../@progress/kendo-data-query/dist/es/main.js");
var magazineViewModel_1 = __webpack_require__("../../../../../src/app/models/magazineViewModel.ts");
var magazine_service_1 = __webpack_require__("../../../../../src/app/service/magazine.service.ts");
var user_1 = __webpack_require__("../../../../../src/app/models/user.ts");
var authentication_service_1 = __webpack_require__("../../../../../src/app/service/authentication.service.ts");
var MagazineViewModelComponent = /** @class */ (function () {
    function MagazineViewModelComponent(magazineService, authenticationService) {
        this.magazineService = magazineService;
        this.authenticationService = authenticationService;
        this.gridState = {
            sort: [],
            skip: 0,
            take: 10
        };
        this.role = false;
        this.userName = "not authorized";
    }
    MagazineViewModelComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authenticationService.auth().subscribe(function (result) {
            var user = result;
            if (user.role === user_1.UserRoles.admin) {
                _this.role = true;
            }
            if (user.userName != null) {
                _this.userName = user.userName;
            }
            ;
        });
        this.view = this.magazineService.map(function (data) { return kendo_data_query_1.process(data, _this.gridState); });
        this.magazineService.read();
    };
    MagazineViewModelComponent.prototype.onStateChange = function (state) {
        this.gridState = state;
        this.magazineService.read();
    };
    MagazineViewModelComponent.prototype.addHandler = function () {
        this.editDataItem = new magazineViewModel_1.MagazineViewModel();
        this.isNew = true;
    };
    MagazineViewModelComponent.prototype.editHandler = function (_a) {
        var dataItem = _a.dataItem;
        this.editDataItem = dataItem;
        this.isNew = false;
    };
    MagazineViewModelComponent.prototype.cancelHandler = function () {
        this.editDataItem = undefined;
    };
    MagazineViewModelComponent.prototype.saveHandler = function (product) {
        this.magazineService.save(product, this.isNew);
    };
    MagazineViewModelComponent.prototype.removeHandler = function (_a) {
        var dataItem = _a.dataItem;
        this.magazineService.remove(dataItem);
    };
    MagazineViewModelComponent = __decorate([
        core_1.Component({
            selector: 'app-magazine-view-model',
            template: __webpack_require__("../../../../../src/app/magazine-view-model/magazine-view-model.component.html"),
            styles: [__webpack_require__("../../../../../src/app/magazine-view-model/magazine-view-model.component.css"), __webpack_require__("../../../../@progress/kendo-theme-default/dist/all.css")],
            encapsulation: core_1.ViewEncapsulation.None,
            providers: [magazine_service_1.MagazineService, authentication_service_1.AuthenticationService]
        }),
        __metadata("design:paramtypes", [magazine_service_1.MagazineService, authentication_service_1.AuthenticationService])
    ], MagazineViewModelComponent);
    return MagazineViewModelComponent;
}());
exports.MagazineViewModelComponent = MagazineViewModelComponent;


/***/ }),

/***/ "../../../../../src/app/models/bookViewModel.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var BookViewModel = /** @class */ (function () {
    function BookViewModel() {
    }
    return BookViewModel;
}());
exports.BookViewModel = BookViewModel;


/***/ }),

/***/ "../../../../../src/app/models/brochureViewModel.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var BrochureViewModel = /** @class */ (function () {
    function BrochureViewModel() {
    }
    return BrochureViewModel;
}());
exports.BrochureViewModel = BrochureViewModel;


/***/ }),

/***/ "../../../../../src/app/models/magazineViewModel.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var MagazineViewModel = /** @class */ (function () {
    function MagazineViewModel() {
    }
    return MagazineViewModel;
}());
exports.MagazineViewModel = MagazineViewModel;


/***/ }),

/***/ "../../../../../src/app/models/user.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var UserAuth = /** @class */ (function () {
    function UserAuth() {
    }
    return UserAuth;
}());
exports.UserAuth = UserAuth;
var UserRoles;
(function (UserRoles) {
    UserRoles[UserRoles["admin"] = 0] = "admin";
    UserRoles[UserRoles["user"] = 1] = "user";
    UserRoles[UserRoles["guest"] = 2] = "guest";
})(UserRoles = exports.UserRoles || (exports.UserRoles = {}));


/***/ }),

/***/ "../../../../../src/app/service/alert.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var Subject_1 = __webpack_require__("../../../../rxjs/_esm5/Subject.js");
var AlertService = /** @class */ (function () {
    function AlertService(router) {
        var _this = this;
        this.router = router;
        this.subject = new Subject_1.Subject();
        this.keepAfterNavigationChange = false;
        router.events.subscribe(function (event) {
            if (event instanceof router_1.NavigationStart) {
                if (_this.keepAfterNavigationChange) {
                    _this.keepAfterNavigationChange = false;
                }
                else {
                    _this.subject.next();
                }
            }
        });
    }
    AlertService.prototype.success = function (message, keepAfterNavigationChange) {
        if (keepAfterNavigationChange === void 0) { keepAfterNavigationChange = false; }
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({ type: 'success', text: message });
    };
    AlertService.prototype.error = function (message, keepAfterNavigationChange) {
        if (keepAfterNavigationChange === void 0) { keepAfterNavigationChange = false; }
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.subject.next({ type: 'error', text: message });
    };
    AlertService.prototype.getMessage = function () {
        return this.subject.asObservable();
    };
    AlertService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [router_1.Router])
    ], AlertService);
    return AlertService;
}());
exports.AlertService = AlertService;


/***/ }),

/***/ "../../../../../src/app/service/auth-guard.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var Observable_1 = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var user_1 = __webpack_require__("../../../../../src/app/models/user.ts");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var authentication_service_1 = __webpack_require__("../../../../../src/app/service/authentication.service.ts");
var alert_service_1 = __webpack_require__("../../../../../src/app/service/alert.service.ts");
var AuthGuard = /** @class */ (function () {
    function AuthGuard(authenticationService, router, alertService) {
        this.authenticationService = authenticationService;
        this.router = router;
        this.alertService = alertService;
        this.isAdmin = false;
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        var _this = this;
        return this.authenticationService.auth().map(function (result) {
            var user = result;
            if (user.role != user_1.UserRoles.admin) {
                _this.isAdmin = false;
                _this.alertService.error("Вход запрещен!");
            }
            if (user.role == user_1.UserRoles.admin) {
                _this.isAdmin = true;
            }
            return _this.isAdmin;
        }).catch(function () {
            _this.router.navigate(['/login']);
            return Observable_1.Observable.of(false);
        });
        ;
    };
    AuthGuard = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [authentication_service_1.AuthenticationService, router_1.Router, alert_service_1.AlertService])
    ], AuthGuard);
    return AuthGuard;
}());
exports.AuthGuard = AuthGuard;


/***/ }),

/***/ "../../../../../src/app/service/authentication.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var http_1 = __webpack_require__("../../../http/esm5/http.js");
__webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
var app_config_1 = __webpack_require__("../../../../../src/app/app.config.ts");
var http_2 = __webpack_require__("../../../common/esm5/http.js");
var platform_browser_1 = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
var AuthenticationService = /** @class */ (function () {
    function AuthenticationService(http, config, httpClient, document) {
        this.http = http;
        this.config = config;
        this.httpClient = httpClient;
        this.document = document;
        var domain = this.document.location.hostname;
        this.url = "http://" + domain;
    }
    AuthenticationService.prototype.login = function (username, password) {
        return this.http.post(this.config.apiUrl + '/token', { username: username, password: password })
            .map(function (response) {
            var user = response.json();
            if (user && user.access_token) {
                sessionStorage.setItem('access_token', JSON.stringify(user.access_token));
            }
        });
    };
    AuthenticationService.prototype.auth = function () {
        var token = JSON.parse(sessionStorage.getItem('access_token'));
        var result = this.httpClient.get(this.url + "/getrole", { headers: { 'Authorization': 'Bearer ' + token } })
            .map(function (res) { return res; });
        return result;
    };
    AuthenticationService.prototype.logout = function () {
        sessionStorage.removeItem('access_token');
        var result = this.httpClient.get(this.url + "/logout")
            .map(function (res) { return res; });
        return result;
    };
    AuthenticationService = __decorate([
        core_1.Injectable(),
        __param(3, core_1.Inject(platform_browser_1.DOCUMENT)),
        __metadata("design:paramtypes", [http_1.Http, app_config_1.AppConfig, http_2.HttpClient, Object])
    ], AuthenticationService);
    return AuthenticationService;
}());
exports.AuthenticationService = AuthenticationService;


/***/ }),

/***/ "../../../../../src/app/service/book.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var http_1 = __webpack_require__("../../../common/esm5/http.js");
var BehaviorSubject_1 = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
var platform_browser_1 = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
var BookService = /** @class */ (function (_super) {
    __extends(BookService, _super);
    function BookService(http, document) {
        var _this = _super.call(this, []) || this;
        _this.http = http;
        _this.document = document;
        _this.data = [];
        var domain = _this.document.location.hostname;
        console.log(domain);
        _this.url = "http://" + domain + "/api/books";
        return _this;
    }
    BookService.prototype.read = function () {
        var _this = this;
        if (this.data.length) {
            return _super.prototype.next.call(this, this.data);
        }
        this.http.get(this.url).map(function (res) { return res; })
            .do(function (data) {
            _this.data = data;
        })
            .subscribe(function (data) {
            _super.prototype.next.call(_this, data);
        });
    };
    BookService.prototype.save = function (data, isNew) {
        var _this = this;
        if (isNew) {
            var model = {
                id: isNew ? 0 : data.id,
                author: data.author,
                name: data.name,
                dateFrom: new Date(),
                genre: data.genre,
                price: data.price
            };
            this.reset();
            return this.http.post(this.url, model)
                .subscribe(function () { return _this.read(); }, function () { return _this.read(); });
        }
        if (!isNew) {
            var model = {
                id: isNew ? 0 : data.id,
                author: data.author,
                name: data.name,
                dateFrom: new Date(),
                genre: data.genre,
                price: data.price
            };
            this.reset();
            return this.http.put(this.url + '/' + data.id, model)
                .subscribe(function () { return _this.read(); }, function () { return _this.read(); });
        }
    };
    BookService.prototype.remove = function (data) {
        var _this = this;
        this.reset();
        var id = data.id;
        return this.http.delete(this.url + '/' + id)
            .subscribe(function () { return _this.read(); }, function () { return _this.read(); });
    };
    BookService.prototype.resetItem = function (dataItem) {
        if (!dataItem) {
            return;
        }
        var originalDataItem = this.data.find(function (item) { return item.ProductID === dataItem.ProductID; });
        Object.assign(originalDataItem, dataItem);
        _super.prototype.next.call(this, this.data);
    };
    BookService.prototype.reset = function () {
        this.data = [];
    };
    BookService.prototype.serializeModels = function (data) {
        return data ? "&models=" + JSON.stringify([data]) : '';
    };
    BookService = __decorate([
        core_1.Injectable(),
        __param(1, core_1.Inject(platform_browser_1.DOCUMENT)),
        __metadata("design:paramtypes", [http_1.HttpClient, Object])
    ], BookService);
    return BookService;
}(BehaviorSubject_1.BehaviorSubject));
exports.BookService = BookService;


/***/ }),

/***/ "../../../../../src/app/service/brochure.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var http_1 = __webpack_require__("../../../common/esm5/http.js");
var BehaviorSubject_1 = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
var platform_browser_1 = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
var BrochureService = /** @class */ (function (_super) {
    __extends(BrochureService, _super);
    function BrochureService(http, document) {
        var _this = _super.call(this, []) || this;
        _this.http = http;
        _this.document = document;
        _this.data = [];
        var domain = _this.document.location.hostname;
        console.log(domain);
        _this.url = "http://" + domain + "/api/brochures";
        return _this;
    }
    BrochureService.prototype.read = function () {
        var _this = this;
        if (this.data.length) {
            return _super.prototype.next.call(this, this.data);
        }
        this.http.get(this.url).map(function (res) { return res; })
            .do(function (data) {
            _this.data = data;
        })
            .subscribe(function (data) {
            _super.prototype.next.call(_this, data);
        });
    };
    BrochureService.prototype.save = function (data, isNew) {
        var _this = this;
        if (isNew) {
            var model = {
                id: isNew ? 0 : data.id,
                name: data.name,
                dateFrom: new Date(),
                genre: data.genre,
                price: data.price
            };
            this.reset();
            return this.http.post(this.url, model)
                .subscribe(function () { return _this.read(); }, function () { return _this.read(); });
        }
        if (!isNew) {
            var model = {
                id: isNew ? 0 : data.id,
                name: data.name,
                dateFrom: new Date(),
                genre: data.genre,
                price: data.price
            };
            this.reset();
            return this.http.put(this.url + '/' + data.id, model)
                .subscribe(function () { return _this.read(); }, function () { return _this.read(); });
        }
    };
    BrochureService.prototype.remove = function (data) {
        var _this = this;
        this.reset();
        var id = data.id;
        return this.http.delete(this.url + '/' + id)
            .subscribe(function () { return _this.read(); }, function () { return _this.read(); });
    };
    BrochureService.prototype.resetItem = function (dataItem) {
        if (!dataItem) {
            return;
        }
        var originalDataItem = this.data.find(function (item) { return item.ProductID === dataItem.ProductID; });
        Object.assign(originalDataItem, dataItem);
        _super.prototype.next.call(this, this.data);
    };
    BrochureService.prototype.reset = function () {
        this.data = [];
    };
    BrochureService.prototype.serializeModels = function (data) {
        return data ? "&models=" + JSON.stringify([data]) : '';
    };
    BrochureService = __decorate([
        core_1.Injectable(),
        __param(1, core_1.Inject(platform_browser_1.DOCUMENT)),
        __metadata("design:paramtypes", [http_1.HttpClient, Object])
    ], BrochureService);
    return BrochureService;
}(BehaviorSubject_1.BehaviorSubject));
exports.BrochureService = BrochureService;


/***/ }),

/***/ "../../../../../src/app/service/home.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var http_1 = __webpack_require__("../../../common/esm5/http.js");
var BehaviorSubject_1 = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
var platform_browser_1 = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
var HomeService = /** @class */ (function (_super) {
    __extends(HomeService, _super);
    function HomeService(http, document) {
        var _this = _super.call(this, []) || this;
        _this.http = http;
        _this.document = document;
        _this.data = [];
        var domain = _this.document.location.hostname;
        console.log(domain);
        _this.url = "http://" + domain + "/api/home";
        return _this;
    }
    HomeService.prototype.get = function () {
        return this.http.get(this.url)
            .subscribe();
    };
    HomeService.prototype.read = function () {
        var _this = this;
        if (this.data.length) {
            return _super.prototype.next.call(this, this.data);
        }
        this.http.get(this.url).map(function (res) { return res; })
            .do(function (data) {
            _this.data = data;
        })
            .subscribe(function (data) {
            _super.prototype.next.call(_this, data);
        });
    };
    HomeService.prototype.resetItem = function (dataItem) {
        if (!dataItem) {
            return;
        }
        var originalDataItem = this.data.find(function (item) { return item.ProductID === dataItem.ProductID; });
        Object.assign(originalDataItem, dataItem);
        _super.prototype.next.call(this, this.data);
    };
    HomeService.prototype.reset = function () {
        this.data = [];
    };
    HomeService.prototype.serializeModels = function (data) {
        return data ? "&models=" + JSON.stringify([data]) : '';
    };
    HomeService = __decorate([
        core_1.Injectable(),
        __param(1, core_1.Inject(platform_browser_1.DOCUMENT)),
        __metadata("design:paramtypes", [http_1.HttpClient, Object])
    ], HomeService);
    return HomeService;
}(BehaviorSubject_1.BehaviorSubject));
exports.HomeService = HomeService;


/***/ }),

/***/ "../../../../../src/app/service/magazine.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var http_1 = __webpack_require__("../../../common/esm5/http.js");
var BehaviorSubject_1 = __webpack_require__("../../../../rxjs/_esm5/BehaviorSubject.js");
var platform_browser_1 = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
var MagazineService = /** @class */ (function (_super) {
    __extends(MagazineService, _super);
    function MagazineService(http, document) {
        var _this = _super.call(this, []) || this;
        _this.http = http;
        _this.document = document;
        _this.data = [];
        var domain = _this.document.location.hostname;
        console.log(domain);
        _this.url = "http://" + domain + "/api/magazines";
        return _this;
    }
    MagazineService.prototype.get = function () {
        return this.http.get(this.url)
            .subscribe();
    };
    MagazineService.prototype.read = function () {
        var _this = this;
        if (this.data.length) {
            return _super.prototype.next.call(this, this.data);
        }
        this.http.get(this.url).map(function (res) { return res; })
            .do(function (data) {
            _this.data = data;
        })
            .subscribe(function (data) {
            _super.prototype.next.call(_this, data);
        });
    };
    MagazineService.prototype.save = function (data, isNew) {
        var _this = this;
        if (isNew) {
            var model = {
                id: isNew ? 0 : data.id,
                name: data.name,
                dateFrom: new Date(),
                genre: data.genre,
                price: data.price
            };
            this.reset();
            return this.http.post(this.url, model)
                .subscribe(function () { return _this.read(); }, function () { return _this.read(); });
        }
        if (!isNew) {
            var model = {
                id: isNew ? 0 : data.id,
                name: data.name,
                dateFrom: new Date(),
                genre: data.genre,
                price: data.price
            };
            this.reset();
            return this.http.put(this.url + '/' + data.id, model)
                .subscribe(function () { return _this.read(); }, function () { return _this.read(); });
        }
    };
    MagazineService.prototype.remove = function (data) {
        var _this = this;
        this.reset();
        var id = data.id;
        return this.http.delete(this.url + '/' + id)
            .subscribe(function () { return _this.read(); }, function () { return _this.read(); });
    };
    MagazineService.prototype.resetItem = function (dataItem) {
        if (!dataItem) {
            return;
        }
        var originalDataItem = this.data.find(function (item) { return item.ProductID === dataItem.ProductID; });
        Object.assign(originalDataItem, dataItem);
        _super.prototype.next.call(this, this.data);
    };
    MagazineService.prototype.reset = function () {
        this.data = [];
    };
    MagazineService.prototype.serializeModels = function (data) {
        return data ? "&models=" + JSON.stringify([data]) : '';
    };
    MagazineService = __decorate([
        core_1.Injectable(),
        __param(1, core_1.Inject(platform_browser_1.DOCUMENT)),
        __metadata("design:paramtypes", [http_1.HttpClient, Object])
    ], MagazineService);
    return MagazineService;
}(BehaviorSubject_1.BehaviorSubject));
exports.MagazineService = MagazineService;


/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
Object.defineProperty(exports, "__esModule", { value: true });
exports.environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var platform_browser_dynamic_1 = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
var app_module_1 = __webpack_require__("../../../../../src/app/app.module.ts");
var environment_1 = __webpack_require__("../../../../../src/environments/environment.ts");
if (environment_1.environment.production) {
    core_1.enableProdMode();
}
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(app_module_1.AppModule)
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map