﻿using System.Collections.Generic;
using AutoMapper;
using LibraryAngular.DAL.Repository;
using LibraryAngular.Entities;
using LibraryAngular.Views.MagazineViewModels;

namespace LibraryAngular.BLL.Services
{
  public class MagazineService
  {
    private MagazinesRepository _magazinesRepository;


    public MagazineService(string connectionString)
    {
      _magazinesRepository = new MagazinesRepository(connectionString);
    }


    public List<MagazineViewModel> GetMagazines()
    {
      var magazines = _magazinesRepository.GetMagazines();

      var bookViewModel = Mapper.Map<IEnumerable<Magazine>, List<MagazineViewModel>>(magazines);

      return (bookViewModel);
    }

    public MagazineViewModel Find(int? id)
    {
      var magazine = _magazinesRepository.Find(id);
      var magazineViewModel = Mapper.Map<Magazine, MagazineViewModel>(magazine);
      return magazineViewModel;
    }

    public void Create(MagazineViewModel bookViewModel)
    {

      var magazine = Mapper.Map<MagazineViewModel, Magazine>(bookViewModel);

      _magazinesRepository.Create(magazine);
    }
    public void Update(MagazineViewModel magazineViewModel)
    {

      var magazine = Mapper.Map<MagazineViewModel, Magazine>(magazineViewModel);

      _magazinesRepository.Update(magazine);
    }

    public void Delete(int id)
    {
      _magazinesRepository.Delete(id);
    }
  }
}