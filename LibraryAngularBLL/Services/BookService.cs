﻿using AutoMapper;
using LibraryAngular.DAL.Repository;
using LibraryAngular.Entities;
using LibraryAngular.Views.BookViewModels;
using System.Collections.Generic;

namespace LibraryAngular.BLL.Services
{
  public class BookService
  {
    private BooksRepository _booksRepository;

    public BookService(string connectionString)
    {
      _booksRepository = new BooksRepository(connectionString);
    }


    public List<BookViewModel> GetBooks()
    {
      var books = _booksRepository.GetBooks();
      var bookViewModel = Mapper.Map<IEnumerable<Book>, List<BookViewModel>>(books);
      return (bookViewModel);
    }

    public BookViewModel Find(int? id)
    {
      var book = _booksRepository.Find(id);
      var bookViewModel = Mapper.Map<Book, BookViewModel>(book);
      return bookViewModel;
    }

    public void Create(BookViewModel bookViewModel)
    {
      var book = Mapper.Map<BookViewModel, Book>(bookViewModel);
      _booksRepository.Create(book);
    }
    public void Update(BookViewModel bookViewModel)
    {
      var book = Mapper.Map<BookViewModel, Book>(bookViewModel);
      _booksRepository.Update(book);
    }

    public void Delete(int id)
    {
      _booksRepository.Delete(id);
    }

  }
}