﻿using AutoMapper;
using LibraryAngular.DAL.Repository;
using LibraryAngular.Entities;
using LibraryAngular.Views.BrochureViewModels;
using System.Collections.Generic;

namespace LibraryAngular.BLL.Services
{
  public class BrochureService
  {
    BrochuresRepository _brochuresRepository;

    public BrochureService(string connectionString)
    {
      _brochuresRepository = new BrochuresRepository(connectionString);
    }
    

    public List<BrochureViewModel> GetBrochures()
    {
      var brochures = _brochuresRepository.GetBrochures();
      var brochuresViewModel = Mapper.Map<IEnumerable<Brochure>, List<BrochureViewModel>>(brochures);
      return brochuresViewModel;
    }

    public BrochureViewModel Find(int? id)
    {
      var brochure = _brochuresRepository.Find(id);
      var brochureViewModel = Mapper.Map<Brochure, BrochureViewModel>(brochure);
      return brochureViewModel;
    }

    public void Create(BrochureViewModel brochuresViewModel)
    {
      var brochure = Mapper.Map<BrochureViewModel, Brochure>(brochuresViewModel);

      _brochuresRepository.Create(brochure);
    }
    public void Update(BrochureViewModel bookViewModel)
    {

      var brochure = Mapper.Map<BrochureViewModel, Brochure>(bookViewModel);

      _brochuresRepository.Update(brochure);
    }

    public void Delete(int id)
    {
      _brochuresRepository.Delete(id);
    }


  }
}