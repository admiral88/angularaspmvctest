﻿using AutoMapper;
using System.Collections.Generic;
using LibraryAngular.Entities;
using LibraryAngular.Views.StoreViewModels;
using LibraryAngular.DAL.Repository;

namespace LibraryAngular.BLL.Services
{
  public class StoreService
  {
    private BooksRepository _booksRepository;
    private MagazinesRepository _magazinesRepository;
    private BrochuresRepository _brochuresRepository;

    public StoreService(string connectionString)
    {
      _booksRepository = new BooksRepository(connectionString);
      _magazinesRepository = new MagazinesRepository(connectionString);
      _brochuresRepository = new BrochuresRepository(connectionString);
    }

    public List<StoreViewModel> GetMappedBooks()
    {
      List<StoreViewModel> mappedBooks = new List<StoreViewModel>();
      var books = _booksRepository.GetBooks();
      mappedBooks = Mapper.Map<List<Book>, List<StoreViewModel>>(books);
      return mappedBooks;
    }

    public List<StoreViewModel> GetMappedBrochures()
    {
      var brochures = _brochuresRepository.GetBrochures();
      var mappedBroshures = Mapper.Map<IEnumerable<Brochure>, List<StoreViewModel>>(brochures);

      return mappedBroshures;
    }
    public List<StoreViewModel> GetMappedMagazines()
    {
      var magazines = _magazinesRepository.GetMagazines();
      var mappedMagazines = Mapper.Map<IEnumerable<Magazine>, List<StoreViewModel>>(magazines);

      return mappedMagazines;
    }

    public List<StoreViewModel> GetViewModelStore()
    {
      var result = GetMappedBooks();
      result.AddRange(GetMappedBrochures());
      result.AddRange(GetMappedMagazines());
      return result;
    }
  }
}