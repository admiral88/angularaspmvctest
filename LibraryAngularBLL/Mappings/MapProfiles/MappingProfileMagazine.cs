﻿using AutoMapper;
using LibraryAngular.Entities;
using LibraryAngular.Views;
using LibraryAngular.Views.MagazineViewModels;

namespace LibraryAngular.BLL.Mappings.MapProfiles
{
  public class MappingProfileMagazine : Profile
  {
    public MappingProfileMagazine()
    {
      CreateMap<MagazineViewModel, Magazine>();
      CreateMap<Magazine, MagazineViewModel>();
    }
  }
}