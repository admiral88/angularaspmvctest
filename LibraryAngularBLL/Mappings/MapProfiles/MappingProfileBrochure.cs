﻿using AutoMapper;
using LibraryAngular.Entities;
using LibraryAngular.Views.BrochureViewModels;

namespace LibraryAngular.BLL.Mappings.MapProfiles
{
  public class MappingProfileBrochure : Profile
  {
    public MappingProfileBrochure()
    {
      CreateMap<BrochureViewModel, Brochure>();
      CreateMap<Brochure, BrochureViewModel>();
    }
  }
}