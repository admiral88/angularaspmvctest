﻿using AutoMapper;
using LibraryAngular.Entities;
using LibraryAngular.Entities.Enums;
using LibraryAngular.Views.StoreViewModels;

namespace LibraryAngular.BLL.Mappings.MapProfiles
{
  public class MappingProfileBrochuresToViewModel : Profile
  {
    public MappingProfileBrochuresToViewModel()
    {
       CreateMap<Brochure, StoreViewModel>() 
      .ForMember("Theme", opt => opt.MapFrom(c => c.Genre))
      .ForMember("TypeProduct", c => c.UseValue(ProductType.Brochures.ToString("F")));;
    }
  }
}