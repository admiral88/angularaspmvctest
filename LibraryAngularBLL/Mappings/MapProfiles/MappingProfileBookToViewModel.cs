﻿using AutoMapper;
using LibraryAngular.Entities;
using LibraryAngular.Entities.Enums;
using LibraryAngular.Views.StoreViewModels;

namespace LibraryAngular.BLL.Mappings.MapProfiles
{
  public class MappingProfileBookToViewModel : Profile
  {
    public MappingProfileBookToViewModel()
    {
      CreateMap<Book, StoreViewModel>()
      .ForMember(c => c.Theme, opt => opt.MapFrom(c => c.Genre))
      .ForMember(c=>c.TypeProduct, opt => opt.UseValue(ProductType.Books.ToString("F")));
    }
  }
}