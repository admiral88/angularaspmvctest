﻿using AutoMapper;
using LibraryAngular.Entities;
using LibraryAngular.Views.BookViewModels;

namespace LibraryAngular.BLL.Mappings.MapProfiles
{
  public class MappingProfileBook : Profile
  {
    public MappingProfileBook()
    {
      CreateMap<Book, BookViewModel>();
      CreateMap<BookViewModel, Book>();
    }
  }
}