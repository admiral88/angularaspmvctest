﻿using AutoMapper;
using LibraryAngular.Entities;
using LibraryAngular.Entities.Enums;
using LibraryAngular.Views.StoreViewModels;

namespace LibraryAngular.BLL.Mappings.MapProfiles
{
  public class MappingProfileMagazinesToViewModel : Profile
  {
    public MappingProfileMagazinesToViewModel()
    {
      CreateMap<Magazine, StoreViewModel>()
        .ForMember("Theme", opt => opt.MapFrom(c => c.Genre))
      .ForMember("TypeProduct", c => c.UseValue(ProductType.Magazines.ToString("F")));
    }
  }
}